#include "include/gameScreen.h"
#include "include/menuScreen.h"
#include "include/overScreen.h"

#include "include/pad.h"
#include "include/PlayableCharacters.h"

#include "include/localMapState.h"
#include "include/battleState.h"
#include "include/eventState.h"
#include "include/worldMapState.h"

#include <stdio.h>
#include <malloc.h>

#include <gsKit.h>
#include <dmaKit.h>
#include <gsToolkit.h>

extern StateMachine GameMachineState;

extern Controller PlaystationGamePad;

extern Actor* Actory[4];

IngameStateMachine IngameMachineState;


// This part is where active Party members are listed
u16 ActiveMembers[4]; // Index 256 means no party member


void GameStart(GSGLOBAL* gsGlobal)
{
	ActiveMembers[0] = 0;
	ActiveMembers[1] = 256;
	ActiveMembers[2] = 256;
	ActiveMembers[3] = 256;
	
	IngameStateMachineStart(&IngameMachineState, &LocalMapState, gsGlobal);
}

void GameUpdate(GSGLOBAL* gsGlobal)
{	
	IngameStateMachineUpdate(&IngameMachineState, gsGlobal);	
}

void GameDraw(GSGLOBAL* gsGlobal, u64 colour)
{
	IngameStateMachineDraw(&IngameMachineState, gsGlobal,colour);	
}

void GameEnd(GSGLOBAL* gsGlobal)
{

}

StateManager GameState =
{
	GameStart,
	GameUpdate,
	GameDraw,
	GameEnd
};

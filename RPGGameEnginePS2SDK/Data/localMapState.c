#include "include/localMapState.h"
#include "include/battleState.h"
#include "include/eventState.h"
#include "include/worldMapState.h"

#include "include/localMapManager.h"

#include "include/pad.h"

#include <stdio.h>
#include <malloc.h>

#include <gsKit.h>
#include <dmaKit.h>
#include <gsToolkit.h>

extern IngameStateMachine IngameMachineState;

extern Controller PlaystationGamePad;

void LocalStart(GSGLOBAL* gsGlobal)
{
	//printf("Initialising Local map Game State\n");
	
	changeMap(gsGlobal,0,0);
}

void LocalUpdate(GSGLOBAL* gsGlobal)
{	
	updateMap(0, 0);
}

void LocalDraw(GSGLOBAL* gsGlobal, u64 colour)
{
	// This part here handles thee map that's being drawn.
	DrawMap(gsGlobal,colour);
}

void LocalEnd(GSGLOBAL* gsGlobal)
{
	// Mandatory Cleanup of the VRAM when changing state
	gsKit_vram_clear(gsGlobal);
}

IngameStateManager LocalMapState =
{
	LocalStart,
	LocalUpdate,
	LocalDraw,
	LocalEnd
};

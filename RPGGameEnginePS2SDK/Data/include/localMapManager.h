#ifndef LOCAL_MAP_MANAGER
#define LOCAL_MAP_MANAGER

#include <gsKit.h>
#include <dmaKit.h>
#include <gsToolkit.h>
#include <stdio.h>

#define MAP000 "Data/Maps/TestMap"


#define TILESET_TOWN "Graphics/Tilesets/TilesetTown.png"


#define TILE_WIDTH 32
#define TILE_HEIGHT 32

void changeMap(GSGLOBAL* gsGlobal, u16 number, u16 tileset);
void updateMap(u16 playerPosX, u16 playerPosY);
void DrawMap(GSGLOBAL* gsGlobal,u64 colour);
void LoadMapIntoArray(char* path, u16 map[256][256]);



#endif

#ifndef BATTLE_STATE
#define BATTLE_STATE

#include "ingameStateManager.h"


void BattleStart(GSGLOBAL* gsGlobal);
void BattleUpdate(GSGLOBAL* gsGlobal);
void BattleDraw(GSGLOBAL* gsGlobal, u64 colour);
void BattleEnd(GSGLOBAL* gsGlobal);

extern IngameStateManager BattleState;

#endif

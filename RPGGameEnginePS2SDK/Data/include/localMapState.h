#ifndef LOCAL_STATE
#define WORLD_STATE

#include "ingameStateManager.h"


void WorldStart(GSGLOBAL* gsGlobal);
void WorldUpdate(GSGLOBAL* gsGlobal);
void WorldDraw(GSGLOBAL* gsGlobal, u64 colour);
void WorldEnd(GSGLOBAL* gsGlobal);

extern IngameStateManager LocalMapState;

#endif

#ifndef INGAME_STATE_MANAGER
#define INGAME_STATE_MANAGER

#include <gsKit.h>
#include <dmaKit.h>
#include <gsToolkit.h>


typedef void _voidCallBack();

typedef struct
{
    _voidCallBack *Start;

    _voidCallBack *Update;

    _voidCallBack *Draw;

    _voidCallBack *End;

    int TransisionIn;

    int TransisionOut;
}IngameStateManager;



typedef struct
{
    int TransisionOutFrames;
    int TransisionInFrames;

    IngameStateManager* CurrentState;
    IngameStateManager* ChangeTo;

}IngameStateMachine;



void IngameStateMachineStart(IngameStateMachine* machine, IngameStateManager* state, GSGLOBAL* gsGlobal);
void IngameStateMachineChange(IngameStateMachine* machine, IngameStateManager* state, GSGLOBAL* gsGlobal);
void IngameStateMachineUpdate(IngameStateMachine* machine, GSGLOBAL* gsGlobal);
void IngameStateMachineDraw(IngameStateMachine* machine, GSGLOBAL* gsGlobal, u64 colour);

#endif

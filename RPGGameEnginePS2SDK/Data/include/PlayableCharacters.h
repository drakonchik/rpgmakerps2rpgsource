#ifndef INGAME_PLAYABLE_CHARACTERS
#define INGAME_PLAYABLE_CHARACTERS

#include <gsKit.h>
#include <dmaKit.h>
#include <gsToolkit.h>
#include <stdio.h>
#include <stdbool.h>

typedef struct 
{
	char* Name;  
    char* Nick;
    char* Description;
    u16 initialLV;
    u16 maximalLV;
    u16 exp;
    u16 weapon;
    u16 shield;
    u16 helmet;
    u16 armour;
    u16 accessory;
    char* Sprite;
    char* Battler;
    char* Portrait;
    u16 actorClass;
    u16 status;
    bool isAlive;
} Actor;



void InitializeActors();
void UpdateActors(u16 actor[4]);
void ActorBattle(u16 Action, u16 actor[4]);
char* ImageActorMap(u16 Actornr);
char* ImageActorBattle(u16 Actornr);
char* ImageActorMenu(u16 Actornr);
void LevelUP();
#endif


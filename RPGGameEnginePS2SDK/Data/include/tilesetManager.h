#ifndef TILESET_MANAGER
#define TILESET_MANAGER


#include <gsKit.h>
#include <dmaKit.h>
#include <gsToolkit.h>



typedef struct
{
	u16 tileX;
	u16 tileY;
	u16 ID; // 0-ground 1-unmovable 2-Water 3-Air
} Tiles;

void InitializeTileset(GSGLOBAL* gsGlobal, char* tileset, u16 width, u16 height);

void UpdateTileset(GSGLOBAL *gsGlobal);

void DrawTile(GSGLOBAL *gsGlobal, u64 colour, u16 tileID, u16 posX, u16 posY);

#endif

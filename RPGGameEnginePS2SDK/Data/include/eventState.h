#ifndef EVENT_STATE
#define EVENT_STATE

#include "ingameStateManager.h"


void EventStart(GSGLOBAL* gsGlobal);
void EventUpdate(GSGLOBAL* gsGlobal);
void EventDraw(GSGLOBAL* gsGlobal, u64 colour);
void EventEnd(GSGLOBAL* gsGlobal);

extern IngameStateManager EventState;

#endif

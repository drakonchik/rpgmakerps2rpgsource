#ifndef CLASSES
#define CLASSES


typedef struct
{
    char[16] Name;
    u16 spells[20][2]; // Spell Index and Spell Level learn!
    u16 EquipableWeapons[256];
    u16 EquipableArmours[1024];
} Classy;

#endif

#include "include/ingameStateManager.h"

void IngameStateMachineStart(IngameStateMachine* ingameMachine, IngameStateManager* ingamestate, GSGLOBAL* gsGlobal)
{
    if (ingamestate != 0 && ingameMachine != 0)
    {
        ingameMachine->ChangeTo = 0;
        ingameMachine->TransisionOutFrames = -1;
        ingameMachine->TransisionInFrames = -1;

        ingameMachine->CurrentState = ingamestate;
        ingameMachine->CurrentState->Start(gsGlobal);
    }
}


void IngameStateMachineChange(IngameStateMachine* ingameMachine, IngameStateManager* ingamestate, GSGLOBAL* gsGlobal)
{
    if (ingamestate != 0 && ingameMachine != 0)
    {
        ingameMachine->ChangeTo = 0;

        ingameMachine->CurrentState->End(gsGlobal);

        ingameMachine->CurrentState = ingamestate;

        ingameMachine->CurrentState->Start(gsGlobal);
    }
}


void IngameStateMachineUpdate(IngameStateMachine* ingameMachine, GSGLOBAL* gsGlobal)
{
    if (ingameMachine != 0)
    {
        ingameMachine->CurrentState->Update(gsGlobal);
    }
}


void IngameStateMachineDraw(IngameStateMachine* ingameMachine, GSGLOBAL* gsGlobal, u64 colour)
{
    if (ingameMachine != 0)
    {
        ingameMachine->CurrentState->Draw(gsGlobal, colour);
    }
}

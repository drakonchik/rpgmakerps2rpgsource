#include "include/RPGFont.h"


extern char* root;

Fonty DefaultFont[224];

void InitializeRPGFont(GSGLOBAL* gsGlobal)
{
	char* tempRoot = "";
	strncpy(tempRoot, root, strlen(root));
	gsKit_texture_png(gsGlobal, &Tileset, strcat(tempRoot,"Graphics/Fonts/RUSENG.png"));
	TilesetCol = GS_SETREG_RGBAQ(0x80,0x80,0x80,0x80,0x00);
	u16 charWidth = 16;
	u16 charHeight = 16;
	
	s32 w = 0;
	for(u16 i = 0; i < 14; i++)
	{
		for(u16 j = 0; j <= 16; j++)
		{
			DefaultFont[w].posX = j * charWidth; // 0 16 32 48 64 80 
			DefaultFont[w].posY = i * charHeight;
			//printf("%d , %d Tile %d\n",TownTileset[w].tileX,TownTileset[w].tileY,w);
			w++;
		}
	}
	
	
}

void UpdateRPGFont(GSGLOBAL *gsGlobal)
{

}

void DrawRPGFONT(GSGLOBAL *gsGlobal, char* text, u16 posX, u16 posY)
{

}

void ArrayToOutputDecoder()

// This part here is more or less hardcoded, wanted to make it universal, sadly too much work for me to bother
void SetupCharacterTableFromFont()
{
	DefaultFont[0].character = " ";
	DefaultFont[1].character = "!";
	DefaultFont[2].character = "\"";
	DefaultFont[3].character = "#";
	DefaultFont[4].character = "$";
	DefaultFont[5].character = "%";
	DefaultFont[6].character = "&";
	DefaultFont[7].character = "\'";
	DefaultFont[8].character = "(";
	DefaultFont[9].character = ")";
	DefaultFont[10].character = "*";
	DefaultFont[11].character = "+";
	DefaultFont[12].character = ",";
	DefaultFont[13].character = "-";
	DefaultFont[14].character = ".";
	DefaultFont[15].character = "/";
	DefaultFont[16].character = "0";
	DefaultFont[17].character = "1";
	DefaultFont[18].character = "2";
	DefaultFont[19].character = "3";
	DefaultFont[20].character = "4";
	DefaultFont[21].character = "5";
	DefaultFont[22].character = "6";
	DefaultFont[23].character = "7";
	DefaultFont[24].character = "8";
	DefaultFont[25].character = "9";
	DefaultFont[26].character = ":";
	DefaultFont[27].character = ";";
	DefaultFont[28].character = "<";
	DefaultFont[29].character = "=";
	DefaultFont[30].character = ">";
	DefaultFont[31].character = "?";
	DefaultFont[32].character = "@";
	DefaultFont[33].character = "A";
	DefaultFont[34].character = "B";
	DefaultFont[35].character = "C";
	DefaultFont[36].character = "D";
	DefaultFont[37].character = "E";
	DefaultFont[38].character = "F";
	DefaultFont[39].character = "G";
	DefaultFont[40].character = "H";
	DefaultFont[41].character = "I";
	DefaultFont[42].character = "J";
	DefaultFont[43].character = "K";
	DefaultFont[44].character = "L";
	DefaultFont[45].character = "M";
	DefaultFont[46].character = "N";
	DefaultFont[47].character = "O";
	DefaultFont[48].character = "P";
	DefaultFont[49].character = "Q";
	DefaultFont[50].character = "R";
	DefaultFont[51].character = "S";
	DefaultFont[52].character = "T";
	DefaultFont[53].character = "O";
	DefaultFont[54].character = "U";
	DefaultFont[55].character = "V";
	DefaultFont[56].character = "W";
	DefaultFont[57].character = "X";
	DefaultFont[58].character = "Y";
	DefaultFont[59].character = "Z";
	DefaultFont[60].character = "[";
	DefaultFont[61].character = "\\";
	DefaultFont[62].character = "]";
	DefaultFont[63].character = "^";
	DefaultFont[64].character = "_";
	DefaultFont[65].character = "`";
	DefaultFont[66].character = "a";
	DefaultFont[67].character = "b";
	DefaultFont[68].character = "c";
	DefaultFont[69].character = "d";
	DefaultFont[70].character = "e";
	DefaultFont[71].character = "f";
	DefaultFont[72].character = "g";
	DefaultFont[73].character = "h";
	DefaultFont[74].character = "i";
	DefaultFont[75].character = "j";
	DefaultFont[76].character = "k";
	DefaultFont[77].character = "l";
	DefaultFont[78].character = "m";
	DefaultFont[79].character = "n";
	DefaultFont[80].character = "o";
	DefaultFont[81].character = "p";
	DefaultFont[82].character = "q";
	DefaultFont[83].character = "r";
	DefaultFont[84].character = "s";
	DefaultFont[85].character = "t";
	DefaultFont[86].character = "u";
	DefaultFont[87].character = "v";
	DefaultFont[88].character = "w";
	DefaultFont[89].character = "x";
	DefaultFont[90].character = "y";
	DefaultFont[91].character = "z";
	DefaultFont[92].character = "{";
	DefaultFont[93].character = "|";
	DefaultFont[94].character = "}";
	DefaultFont[95].character = "~";
	DefaultFont[96].character = "";
	DefaultFont[97].character = "";
	DefaultFont[98].character = "¡";
	DefaultFont[99].character = "¢";
	DefaultFont[100].character = "£";
	DefaultFont[101].character = "¤";
	DefaultFont[102].character = "¥";
	DefaultFont[103].character = "¦";
	DefaultFont[104].character = "§";
	DefaultFont[105].character = "¨";
	DefaultFont[106].character = "©";
	DefaultFont[107].character = "";
	DefaultFont[108].character = "«";
	DefaultFont[109].character = "¬";
	DefaultFont[110].character = "";
	DefaultFont[111].character = "®";
	DefaultFont[112].character = "";
	DefaultFont[113].character = "°";
	DefaultFont[114].character = "±";
	DefaultFont[115].character = "²";
	DefaultFont[116].character = "³";
	DefaultFont[117].character = "";
	DefaultFont[118].character = "µ";
	DefaultFont[119].character = "¶";
	DefaultFont[120].character = "·";
	DefaultFont[121].character = "÷";
	DefaultFont[122].character = "¹";
	DefaultFont[123].character = "";
	DefaultFont[124].character = "»";
	DefaultFont[125].character = "¼";
	DefaultFont[126].character = "½";
	DefaultFont[127].character = "¾";
	DefaultFont[128].character = "¿";
	DefaultFont[129].character = "";
	DefaultFont[130].character = "Ё";
	DefaultFont[131].character = "";
	DefaultFont[132].character = "";
	DefaultFont[133].character = "";
	DefaultFont[134].character = "";
	DefaultFont[135].character = "";
	DefaultFont[136].character = "";
	DefaultFont[137].character = "";
	DefaultFont[138].character = "";
	DefaultFont[139].character = "";
	DefaultFont[140].character = "";
	DefaultFont[141].character = "";
	DefaultFont[142].character = "";
	DefaultFont[143].character = "";
	DefaultFont[144].character = "";
	DefaultFont[145].character = "А";
	DefaultFont[146].character = "Б";
	DefaultFont[147].character = "В";
	DefaultFont[148].character = "Г";
	DefaultFont[149].character = "Д";
	DefaultFont[150].character = "Е";
	DefaultFont[151].character = "Ж";
	DefaultFont[152].character = "З";
	DefaultFont[153].character = "И";
	DefaultFont[154].character = "Й";
	DefaultFont[155].character = "К";
	DefaultFont[156].character = "Л";
	DefaultFont[157].character = "М";
	DefaultFont[158].character = "Н";
	DefaultFont[159].character = "О";
	DefaultFont[160].character = "П";
	DefaultFont[161].character = "Р";
	DefaultFont[162].character = "С";
	DefaultFont[163].character = "Т";
	DefaultFont[164].character = "У";
	DefaultFont[165].character = "Ф";
	DefaultFont[166].character = "Х";
	DefaultFont[167].character = "Ц";
	DefaultFont[168].character = "Ч";
	DefaultFont[169].character = "Ш";
	DefaultFont[170].character = "Щ";
	DefaultFont[171].character = "Ъ";
	DefaultFont[172].character = "Ы";
	DefaultFont[173].character = "Ь";
	DefaultFont[174].character = "Э";
	DefaultFont[175].character = "Ю";
	DefaultFont[176].character = "Я";
	DefaultFont[177].character = "а";
	DefaultFont[178].character = "б";
	DefaultFont[179].character = "в";
	DefaultFont[180].character = "г";
	DefaultFont[181].character = "д";
	DefaultFont[182].character = "е";
	DefaultFont[183].character = "ж";
	DefaultFont[184].character = "з";
	DefaultFont[185].character = "и";
	DefaultFont[186].character = "й";
	DefaultFont[187].character = "к";
	DefaultFont[188].character = "л";
	DefaultFont[189].character = "м";
	DefaultFont[190].character = "н";
	DefaultFont[191].character = "о";
	DefaultFont[192].character = "п";
	DefaultFont[193].character = "р";
	DefaultFont[194].character = "с";
	DefaultFont[195].character = "т";
	DefaultFont[196].character = "у";
	DefaultFont[197].character = "ф";
	DefaultFont[198].character = "х";
	DefaultFont[199].character = "ц";
	DefaultFont[200].character = "ч";
	DefaultFont[201].character = "ш";
	DefaultFont[202].character = "щ";
	DefaultFont[203].character = "ъ";
	DefaultFont[204].character = "ы";
	DefaultFont[205].character = "ь";
	DefaultFont[206].character = "э";
	DefaultFont[207].character = "ю";
	DefaultFont[208].character = "я";
	DefaultFont[209].character = "";
	DefaultFont[210].character = "ё";
	DefaultFont[211].character = "";
	DefaultFont[212].character = "";
	DefaultFont[213].character = "";
	DefaultFont[214].character = "";
	DefaultFont[215].character = "";
	DefaultFont[216].character = "";
	DefaultFont[217].character = "";
	DefaultFont[218].character = "";
	DefaultFont[219].character = "";
	DefaultFont[220].character = "";
	DefaultFont[221].character = "";
	DefaultFont[222].character = "";
	DefaultFont[223].character = "";
	DefaultFont[224].character = "";	
}

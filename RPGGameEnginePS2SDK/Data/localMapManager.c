#include "include/localMapManager.h"
#include "include/tilesetManager.h"

char* mapPathLayer1;
char* mapPathLayer2;
char* tilesetPath;
u16 mapSizeInformation[2]; //  in Tiles

u16 MAP1[256][256];
u16 MAP2[256][256];

u16 *ptrMAP1;
u16 *ptrMAP2;
extern char* root;

void changeMap(GSGLOBAL* gsGlobal, u16 number, u16 tileset)
{
	switch(number)
	{
		case 0:
			mapPathLayer1 = MAP000;
			mapPathLayer2 = MAP000;
		break;
		default:
			mapPathLayer1 = MAP000;
			mapPathLayer2 = MAP000;
	}
	
	switch(tileset)
	{
		case 0:
			tilesetPath = TILESET_TOWN;
		break;
		default:
			tilesetPath = TILESET_TOWN;
	}
	
	*ptrMAP1 = &MAP1; 
	*ptrMAP2 = &MAP2;
	
	char* tempRoot = "";
	strncpy(tempRoot, root, strlen(root));	
	LoadMapIntoArray(strcat(tempRoot,strcat(mapPathLayer1, "_Layer1.csv")), *ptrMAP1);	
	LoadMapIntoArray(strcat(tempRoot,strcat(mapPathLayer2, "_Layer2.csv")), *ptrMAP2);
	
	InitializeTileset(gsGlobal, strcat(tempRoot, tilesetPath), TILE_WIDTH, TILE_HEIGHT);
}


void updateMap(u16 playerPosX, u16 playerPosY)
{

	

}

void DrawMap(GSGLOBAL* gsGlobal,u64 colour)
{

	// Layer 0 (Ground Layer)	
	for(u16 i = 0; i < mapSizeInformation[0]; i++)
	{
		for(u16 j = 0; j < mapSizeInformation[1]; j++)
		{
			DrawTile(gsGlobal,colour, MAP1[i][j], j * TILE_WIDTH, i * TILE_HEIGHT);
		}
	}

}



void LoadMapIntoArray(char* path, u16 map[256][256])
{
	   char buffer[1024];
	   char *record,*line;
	   int i=0,j=0,s=0;
	   int mapData = 0;
	   FILE *fstream = fopen(path,"r+");
	   
	   if(fstream == NULL)
	   {
		 	return 19 ;
	   }
	   
	   
	   
	   while((line=fgets(buffer,sizeof(buffer),fstream))!=NULL)
	   {
			record = strtok(line,",");	
			while(record != NULL)
			{
				printf("%s_",record);    //here you can put the record into the array as per your requirement.
				if(mapData == 0)
				{
					mapSizeInformation[s] = atoi(record);
					s++;
				}
				else
				{
					map[i][j] = atoi(record);
					j++;
				}
				
				record = strtok(NULL,",");
			}
			mapData = 1;
			i++;
	   }
	   
	   fclose(fstream);
}




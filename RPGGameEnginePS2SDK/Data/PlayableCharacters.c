#include "include/PlayableCharacters.h"

#include <stdio.h>
#include <malloc.h>

#include <gsKit.h>
#include <dmaKit.h>
#include <gsToolkit.h>

Actor Actory[4] =  {{"Drakonchik", "Andy", "", 0,99,0,0,0,0,0,0,"","","",0,0,true},
					{"Joey", "", "Monke", 0,99,0,0,0,0,0,0,"","","",0,0,true},
					{"TnA", "", "Smarty", 0,99,0,0,0,0,0,0,"","","",0,0,true},
					{"krHACKen", "Hacky", "", 0,99,0,0,0,0,0,0,"","","",0,0,true}};
	// Here we write the actor data from seen in a struct
	//Actory[0] = {"Drakonchik", "Andy", "", 0,99,0,0,0,0,0,"","","",0,0,true};
	//Actory[1] = {"Joey", "", "Monke", 0,99,0,0,0,0,0,"","","",0,0,true};
	//Actory[2] = {"TnA", "", "Smarty", 0,99,0,0,0,0,0,"","","",0,0,true};
	//Actory[3] = {"krHACKen", "Hacky", "", 0,99,0,0,0,0,0,"","","",0,0,true};

void InitializeActors()
{

	// Load data from save file if there is one.
}

// Can have up to four active Actors only
void UpdateActors(u16 actor[4])
{
	for(u16 i= 0; i < 4; i++)
	{
		if(Actory[actor[i]].isAlive)
		{
			switch(Actory[actor[i]].status)
			{
				case 0:
					// Healthy
				break;
				
				default:
				
			}
		} 
	}
}

void ActorBattle(u16 Action, u16 actor[4])
{
	switch(Action)
	{
		// Attack
		case 0:
		
		break;
		// Spell
		case 1:
		
		break;
		// Item
		case 2:
		
		break;
		// Guard
		case 3:
		
		break;
		// In case Actor is dead or not in party
		default:
	}
	
	// TODO: Poison and other status effects whenever the actors turn comes
	// Insert from the tool
}

// To not Overburden the Graphics Synthesizer, these functions will only return the required string values for loading the character sprites

// FieldSprite
char* ImageActorMap(u16 Actornr)
{
	return Actory[Actornr].Sprite;
}

char* ImageActorBattle(u16 Actornr)
{
	return Actory[Actornr].Battler;
}

char* ImageActorMenu(u16 Actornr)
{
	return Actory[Actornr].Portrait;
}

